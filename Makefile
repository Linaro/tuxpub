.PHONY:all flake8 check-black black pytest
all: pytest check-black flake8
	@echo "🚀😀👌😍🚀"

pytest:
	pytest --cov-report term-missing --cov-fail-under=100 --cov=tuxpub tests/

check-black:
	black --check .

flake8:
	flake8 .

black:
	black .

